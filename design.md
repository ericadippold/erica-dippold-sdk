# Design Principles

The development of this SDK was based upon three main principles:
- Minimal - goal is to mimize the amount of steps for the developer. The object was to make the SDK as simple and easy to use as possible.
- Understandable - use well-known / intuitive terminology familiar to developers.
- Data ready - have the returned data be cleaned and ready for the developer to integrate.

# Future Considerations and Features

If I had more time to work on this project I would implement the following additions:
- Intelligently handle errors for the developers with intuitive returns (i.e. 404, empty {})
- Prevent mistakes that could occur by validating required fields and having strongly typed parameters
- Combine multiple calls for unique outputs
- Redesign for user to initialize personal access token for testing
